package com.example.myapplication

fun main() {
    println("Hello World")


    val number : Int = 2
    val i:Byte = number.toByte()
    val s:String = number.toString()
    val f:Float = number.toFloat()

    val e = 35_000_000

    println("number is $number, i is ${number.toByte()}")
    println("e + a = ${e + i}")
    println(i)
    println(s)
    println(f)
    println(e)

    val x = 10
    val y = 2
    val c = 87

    if(c>84){
        println("A")
    } else if ( c in 80 .. 79) {
        println("B")
    }

//    val z = c >= downTo >= 0
//    val u = x >= downTo >= 3

    println(x)


    println(
        when (c) {
            in 80 .. 100 -> "A"
            in 70 .. 100 -> "B"
            in 60 .. 100 -> "C"
            in 50 .. 100 -> "D"
            else -> "F"
        }
    )

}